import scrapy


class VtkSpider(scrapy.Spider):
    name = 'vtkspider'
    start_urls = ['https://vtk.ugent.be/career/students/jobs/?page=1']

    custom_settings = {
        'CONCURRENT_REQUESTS': 1,
    }

    def parse(self, response):
        next_href = response.xpath("//*[@class='uk-pagination-next']//@href").get()
        self.logger.info(f"found next_href {next_href}")
        yield scrapy.Request(response.urljoin(next_href), self.parse)

        for tr in response.xpath('//tr'):
            descr_href = tr.xpath('./td[1]//@href').get()
            pubdate = tr.xpath('./td[3]//text()').get()
            self.logger.info(f"found descr_href {descr_href}")
            yield scrapy.Request(response.urljoin(descr_href),
                                 self.parse_descr,
                                 meta={'pubdate': pubdate})

    def parse_descr(self, response):
        descr = {}
        descr['Publication date'] = response.meta['pubdate']
        for tr in response.xpath('//tr'):
            tds = tr.xpath('./td//text()').getall()
            tds = [td.strip() for td in tds]
            k = tds[0]
            v = ' '.join(tds[1:])  # remainder
            descr[k] = v

        yield descr

