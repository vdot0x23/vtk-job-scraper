{ pkgs ? import (fetchTarball { url = "https://github.com/NixOS/nixpkgs/archive/7dc71aef32e8faf065cb171700792cf8a65c152d.tar.gz";
                                sha256 = "sha256:1x2n334nkmpbjli3b0q3qs8hzpr16r8280aivv7zbsg7c31fqhrd"; }) {} }:

pkgs.mkShell {
  buildInputs = [
    pkgs.python
    pkgs.python310Packages.scrapy
  ];
}
